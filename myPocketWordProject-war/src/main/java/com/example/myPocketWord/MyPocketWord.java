package com.example.myPocketWord;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.Key;


@Entity
public class MyPocketWord {
	@Parent Key<MyPocketWordEntity> word;
	@Id public Long id;
	
	
	public String email;
	public String googleKey;
	public String password;
	
	public MyPocketWord() {
		
	}

	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getGoogleKey() {
		return googleKey;
	}

	public void setGoogleKey(String googleKey) {
		this.googleKey = googleKey;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public MyPocketWord(String email, String googlekey, String password) {
		this.googleKey = googlekey;
		this.email = email;
		this.password = password;
	}
	
}
