package com.example.myPocketWord;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckDataImpl implements CheckData {

	MyPocketWord myPocketWord;
	
	private static final Pattern EMAIL_PATTERN = Pattern.compile("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$");
	
	public CheckDataImpl(MyPocketWord myPocketWord) {
		super();
		this.myPocketWord = myPocketWord;
	}

	public MyPocketWord getMyPocketWord() {
		return myPocketWord;
	}



	public void setMyPocketWord(MyPocketWord myPocketWord) {
		this.myPocketWord = myPocketWord;
	}

	private boolean IsNull(String a) {
		return a == null;
	}
	public boolean control() {
		boolean controlMail = controlEmail(myPocketWord.getEmail());
		boolean controlPass = controlPassword(myPocketWord.getPassword());
		boolean controlKey = controlGoogleKey(myPocketWord.getGoogleKey());
		return controlMail & controlPass & controlKey;
	}
	
	private boolean controlGoogleKey(String googleKey) {
		if (IsNull(googleKey)) {
			return false;
		}
		return true;
	}

	private boolean controlPassword(String password) {
		if (IsNull(password)) {
			return false;
		}
		return true;
	}

	private boolean controlEmail(String email) {
		if (IsNull(email)) {
			return false;
		}
		return validate(email);
	}
	
	public boolean validate(String email) {
		Matcher matcher = EMAIL_PATTERN.matcher(email);
		return matcher.matches();
	}

}
