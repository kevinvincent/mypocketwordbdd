package com.example.myPocketWord;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class MyPocketWordEntity {
	@Id public String word;
}
