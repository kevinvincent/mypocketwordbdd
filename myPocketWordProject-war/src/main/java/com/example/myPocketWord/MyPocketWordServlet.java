package com.example.myPocketWord;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.repackaged.com.google.common.base.Charsets;
import com.google.appengine.repackaged.com.google.common.hash.Hashing;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyPocketWordServlet extends HttpServlet{
	  /**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	@Override
	  public void doPost(HttpServletRequest req, HttpServletResponse resp)
	      throws IOException {
		  MyPocketWord myPocketWord;
		  DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		  com.google.appengine.api.datastore.Key acmeKey = KeyFactory.createKey("Company", "Acme");
		  
	      String email = req.getParameter("email");
	      String googleKey = req.getParameter("googleKey");
	      String password = req.getParameter("password");
	      
	      myPocketWord = new MyPocketWord(email, googleKey, password);
	      CheckData check = new CheckDataImpl(myPocketWord);
	      if (check.control()) {
		      Entity data = new Entity("MyPocketWord", acmeKey);
		      data.setProperty("email", myPocketWord.getEmail());
		      data.setProperty("googleKey", myPocketWord.getGoogleKey());
		      String hashPass = Hashing.sha256().hashString(myPocketWord.getPassword(),
		    		  Charsets.UTF_8 ).toString();
		      data.setProperty("password", hashPass);
		      datastore.put(data);
	      }
	  }

}
